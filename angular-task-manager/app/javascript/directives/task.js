/**
 * Directive for task element.
 *
 * @api private
 */

app.directive('taskRow', function() {
  return {
      restrict: 'E'
    , scope: '@'
    , templateUrl: 'javascript/views/partials/task.html'
  };
});