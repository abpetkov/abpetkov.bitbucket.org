/**
 * Directive for single comment.
 *
 * @api private
 */

app.directive('commentRow', function() {
  return {
      restrict: 'E'
    , scope: '@'
    , templateUrl: 'javascript/views/partials/comment.html'
  };
});