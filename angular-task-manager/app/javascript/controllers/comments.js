/**
 * Handle comments.
 *
 * @param {Object} $scope
 * @param {Object} $routeParams
 * @param {Object} TaskComments
 * @api private
 */

app.controller('CommentsHandler', ['$scope', '$routeParams', 'TaskComments', function($scope, $routeParams, TaskComments) {
  var comments = $scope.comments = TaskComments.get();

  $scope.date = new Date();
  $scope.search = $scope.id;

  /**
   * Add new comment.
   *
   * @api private
   */

  $scope.add = function() {
    if(!angular.isUndefined($scope.commentUser) && !angular.isUndefined($scope.commentContent) && $scope.commentUser !== '' && $scope.commentContent !== '') {
      TaskComments.add($scope.commentContent, $routeParams.taskId, $scope.date, $scope.commentUser, true);
      $scope.commentContent = '';
    } else {
      alert('Please fill in both fields.');
    }
  };
}]);
