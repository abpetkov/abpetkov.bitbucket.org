/**
 * Generate task page.
 *
 * @param {Object} $scope
 * @param {Object} $routeParams
 * @param {Object} $TasksList
 * @api private
 */

app.controller('TaskShow', ['$scope', '$routeParams', 'TasksList', function($scope, $routeParams, TasksList) {

  /**
   * Find proper info object.
   *
   * @api private
   */

  angular.forEach(TasksList.get(), function(value, key) {
    if (value.id == $routeParams.taskId) {
      $scope.id = value.id;
      $scope.title = value.title;
      $scope.completed = (value.completed) ? 'Completed' : '';
    } else {
      return false;
    }
  });
}]);