/**
 * Generate task list.
 *
 * @param {Object} $scope
 * @param {Object} TasksList
 * @api private
 */

app.controller('TaskList', ['$scope', 'TasksList', function($scope, TasksList) {
  var tasks = $scope.tasks = TasksList.get();

  /**
   * Add new task handler.
   *
   * @api private
   */

  $scope.add = function() {
    if(!angular.isUndefined($scope.task) && $scope.task !== '') {
      TasksList.add(false, $scope.createGUID(), $scope.task, true);
    } else {
      alert('Please enter a valid task name.');
    }
    $scope.task = '';
  };

  /**
   * Remove task handler.
   *
   * @param {Object} task
   * @api private
   */

  $scope.remove = function(task) {
    TasksList.remove(task);
  };

  /**
   * Complete a task.
   *
   * @param {Object} task
   * @api private
   */

  $scope.complete = function(task) {
    TasksList.complete(task);
  };

  /**
   * Generate GUID.
   *
   * @api private
   */

  $scope.createGUID = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random() * 16|0, v = c === 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    });
  };
}]);