app.config(function ($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'javascript/views/list.html',
    controller: 'TaskList'
  });

  $routeProvider.when('/tasks/:taskId', {
    templateUrl: 'javascript/views/show.html',
    controller: 'TaskShow'
  });

  $routeProvider.otherwise({
    redirectTo: '/'
  });
});
