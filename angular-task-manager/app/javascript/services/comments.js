/**
 * Store comments in localStorage.
 *
 * @returns {Function} add
 * @returns {Function} put
 * @returns {Function} get
 * @api private
 */

app.factory('TaskComments', function() {
  var storageID = 'taskComments'
    , comments = JSON.parse(localStorage.getItem(storageID) || '[]');

  return {

      /**
       * Add new comment object to comments array.
       *
       * @param {String} content
       * @param {String} parent
       * @param {String} time
       * @param {String} user
       * @api private
       */

      add: function(content, parent, time, user, store) {
        comments.push({
            content: content
          , parent: parent
          , time: time
          , user: user
        });

        if (store) this.put(comments);
      }

      /**
       * Store comments array into storage.
       *
       * @param {Object} item
       * @api private
       */

    , put: function(item) {
        localStorage.setItem(storageID, JSON.stringify(item));
      }

      /**
       * Get comments object.
       *
       * @returns {Object} comments
       * @api private
       */

    , get: function() {
        return comments;
      }
  };
});
