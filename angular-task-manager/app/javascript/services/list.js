/**
 * Read and write in localStorage.
 *
 * @returns {Function} add
 * @returns {Function} put
 * @returns {Function} get
 * @returns {Function} remove
 * @returns {Function} complete
 * @api private
 */

app.factory('TasksList', function() {
  var storageID = 'todoTasks'
    , tasks = JSON.parse(localStorage.getItem(storageID) || '[]');

  return {

      /**
       * Add new task object to list array.
       *
       * @param {Boolean} completed
       * @param {String} id
       * @param {String} title
       * @param {Boolean} store
       * @api private
       */

      add: function(completed, id, title, store) {
        tasks.push({
            completed: completed
          , id: id
          , title: title
        });

        if (store) this.put(tasks);
      }

      /**
       * Store tasks array into storage.
       *
       * @param {Object} item
       * @api private
       */

    , put: function(item) {
        localStorage.setItem(storageID, JSON.stringify(item));
      }

      /**
       * Get tasks list.
       *
       * @returns {Object} tasks
       * @api private
       */

    , get: function() {
        return tasks;
      }

      /**
       * Remove task object from task list.
       *
       * @param {Object} task
       * @api private
       */

    , remove: function(task) {
        var action = confirm('Are you sure you want to delete this task permenently?');

        if (action) {
          tasks.splice(tasks.indexOf(task), 1);
          this.put(tasks);
        } else {
          return false;
        }
      }

      /**
       * Mark task as completed.
       *
       * @param {Object} task
       * @api private
       */

    , complete: function(task) {
        if (task.completed === false) task.completed = true;
        else task.completed = false;
        this.put(tasks);
    }
  };
});