###
# Generate task list.
#
# param {Object} $scope
# param {Object} TasksList
# api private
###

app.controller 'TaskList', ['$scope', 'TasksList', ($scope, TasksList) ->
  $scope.tasks = TasksList.get()

  ###
  # Add new task handler.
  #
  # api private
  ###

  $scope.add = ->
    if $scope.task? and $scope.task isnt ''
      TasksList.add false, this.createGUID(), $scope.task, true
      $scope.task = ''
    else alert 'Please enter a valid task name.'

  ###
  # Remove task handler.
  #
  # param {Object} task
  # api private
  ###

  $scope.remove = (task) ->
    TasksList.remove task
    return

  ###
  # Complete a task.
  #
  # param {Object} task
  # api private
  ###

  $scope.complete = (task) ->
    TasksList.complete task
    return

  ###
  # Generate GUID.
  #
  # api private
  ###

  $scope.createGUID = ->
    'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace /[xy]/g, (c) ->
      r = Math.random() * 16|0
      v = if c is 'x' then r else (r&0x3|0x8)
      v.toString 16

  return
]