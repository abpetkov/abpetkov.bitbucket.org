###
# Generate task page.
#
# param {Object} $scope
# param {Object} $routeParams
# param {Object} $TasksList
# api private
###

app.controller 'TaskShow', ['$scope', '$routeParams', 'TasksList', ($scope, $routeParams, TasksList) ->

  ###
  # Find proper info object.
  #
  # api private
  ###

  for task in TasksList.get()
    if task.id is $routeParams.taskId
      $scope.id = task.id
      $scope.title = task.title
      $scope.completed = if task.completed is yes then 'completed' else 'pending'
]