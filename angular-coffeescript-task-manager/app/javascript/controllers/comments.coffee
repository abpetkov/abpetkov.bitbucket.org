###
# Handle comments.
#
# param {Object} $scope
# param {Object} $routeParams
# param {Object} TaskComments
# api private
###

app.controller 'CommentsHandler', ['$scope', '$routeParams', 'TaskComments', ($scope, $routeParams, TaskComments) ->
  $scope.comments = TaskComments.get()
  $scope.date = new Date()
  $scope.search = $scope.id

  ###
  # Add new comment.
  #
  # api private
  ###

  $scope.add = ->
    if $scope.commentUser? and $scope.commentContent? and $scope.commentUser isnt '' and $scope.commentContent isnt ''
      TaskComments.add $scope.commentContent, $routeParams.taskId, $scope.date, $scope.commentUser, yes
      $scope.commentContent = ''
    else
      alert 'Please fill in both fields.'

  return
]