###
# Store comments in localStorage.
#
# returns {Function} add
# returns {Function} put
# returns {Function} get
# api private
###

app.factory 'TaskComments', ->
  new class TaskComments

    ###
    # TaskComments constructor
    ###

    constructor: ->
      @storageID = 'task-comments'
      @comments = JSON.parse localStorage.getItem(@storageID) or '[]'

    ###
    # Add new comment object to comments array.
    #
    # param {String} content
    # param {String} parent
    # param {String} time
    # param {String} user
    # param {Boolean} store
    # api private
    ###

    add: (content, parent, time, user, store) ->
      @comments.push content: content, parent: parent, time: time, user: user
      if store is yes then this.put(@comments)
      return

    ###
    # Store comments array into storage.
    #
    # param {Object} item
    # api private
    ###

    put: (item) ->
      localStorage.setItem @storageID, JSON.stringify item
      return

    ###
    # Get comments object.
    #
    # returns {Object} comments
    # api private
    ###

    get: ->
      @comments