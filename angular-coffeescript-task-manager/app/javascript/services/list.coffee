###
# Interact with localStorage.
#
# returns {Function} add
# returns {Function} put
# returns {Function} get
# returns {Function} remove
# returns {Function} complete
# api private
###

app.factory 'TasksList', ->
  new class TasksList

    ###
    # TasksList constructor.
    ###

    constructor: ->
      @storageID = 'todo-tasks'
      @tasks = JSON.parse localStorage.getItem(@storageID) or '[]'

    ###
    # Add new task object to list array.
    #
    # param {Boolean} completed
    # param {String} id
    # param {String} title
    # param {Boolean} store
    # api private
    ###

    add: (completed, id, title, store) ->
      @tasks.push completed: completed, id: id, title: title
      if store is yes then this.put(@tasks)
      return

    ###
    # Store tasks array into storage.
    #
    # param {Object} item
    # api private
    ###

    put: (item) ->
      localStorage.setItem @storageID, JSON.stringify item
      return

    ###
    # Get tasks list.
    #
    # returns {Object} tasks
    # api private
    ###

    get: ->
      @tasks

    ###
    # Remove task object from task list.
    #
    # param {Object} task
    # api private
    ###

    remove: (task) ->
      action = confirm 'Are you sure you want to delete this task permenently?'

      if action is yes
        @tasks.splice @tasks.indexOf(task), 1
        this.put(@tasks)
        return
      else return false

    ###
    # Mark task as completed.
    #
    # param {Object} task
    # api private
    ###
    complete: (task) ->
      if task.completed is no then task.completed = yes
      else task.completed = no
      this.put @tasks
      return