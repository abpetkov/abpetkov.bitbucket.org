###
# Directive for task element.
#
# api private
###

app.directive 'taskRow', ->
  {
    restrict: 'E'
    scope: '@'
    templateUrl: 'javascript/views/partials/task.html'
  }