###
# Directive for single comment.
#
# api private
###

app.directive 'commentRow', ->
  {
    restrict: 'E'
    scope: '@'
    templateUrl: 'javascript/views/partials/comment.html'
  }